package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoDBImpl:AddressDao{
    override fun findById(id: Long): Address? {
        return addressRepository.findById(id).orElse(null)
    }

    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun getAddresses(): List<Address> {
        return addressRepository.findAll().filterIsInstance(Address::class.java)
    }
}
