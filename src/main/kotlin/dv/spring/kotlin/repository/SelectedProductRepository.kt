package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository : CrudRepository<SelectedProduct, Long>{
    fun findByProduct_Name(Product: String): SelectedProduct?
    fun findByProduct_NameContaining(product: String): List<SelectedProduct>
    fun findByProduct_NameContainingIgnoreCase(product: String): List<SelectedProduct>
    fun findByProduct_NameContainingIgnoreCaseOrQuantityContaining(product: String, quantity: Int): List<SelectedProduct>
    fun findByProduct_NameContainingIgnoreCase(product: String, page: Pageable): Page<SelectedProduct>

}